require_relative 'umm_adapter/primitive_mapping'
module UmmAdapter
  def self.adapter_profile(reload = false)
    if reload
      @cached_umm_profile = _adapter_profile_from_dsl
    else
      @cached_umm_profile ||= _adapter_profile_from_dsl
    end
  end
  
  def self._adapter_profile_from_dsl
    UmlMetamodel.from_dsl_file(File.expand_path('../umm_adapter/umm_adapter_profile.dsl.rb', __FILE__)).first
  end
  
  def self.auxilliary_primitives_package
    UmlMetamodel.from_dsl_file(File.expand_path('../umm_adapter/uml_auxilliary_primitives.dsl.rb', __FILE__)).first
  end  
  
  # Character used to denote where to split concatenated element ids.
  ID_CONCAT_CHAR = '|' unless defined?(UmmAdapter::ID_CONCAT_CHAR)
  def self.split_id(value, splitter = ID_CONCAT_CHAR)
    value.to_s.split(splitter)
  end
end
