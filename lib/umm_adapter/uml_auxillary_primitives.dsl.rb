package "UML", :id => "uml_package" do
  primitive "Datetime", :id => "datetime"
  primitive "RegularExpression", :id => "regular_expression"
  primitive "Time", :id => "time"
  primitive "Uri", :id => "uri"
  applied_stereotype :instance_of => "UmlMetamodelAdapter::metainformation" do
    applied_tag :instance_of => "UmlMetamodelAdapter::metainformation::version", :value => "0.0.0"
  end
end