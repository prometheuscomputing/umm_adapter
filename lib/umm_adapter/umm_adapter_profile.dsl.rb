profile "UmlMetamodelAdapter", :id => "uml_metamodel_adapter_package" do
# profile "UmlMetamodelAdapter", :imports => ["UML"], :id => "uml_metamodel_adapter_package" do
  stereotype "generation_options", :metaclasses => ["Package"], :id => "generation_options" do
    tag "pluralize_role_names", :id => "pluralize_role_names"
    tag "snakecase_role_names", :id => "snakecase_role_names"
  end
  stereotype "metainformation", :metaclasses => ["Element"], :id => "metainformation" do
    tag "project_name", :id => "project_name"
    tag "version", :id => "version"
  end
  applied_stereotype :instance_of => "UmlMetamodelAdapter::metainformation" do
    applied_tag :instance_of => "UmlMetamodelAdapter::metainformation::version", :value => "0.0.0"
  end
end
