module UmmAdapter
  def self.md_to_umm_primitive_mapping(reload = false)
    @md_to_umm_primitive_mapping ||= {}
    @md_to_umm_primitive_mapping = {} if reload
    return @md_to_umm_primitive_mapping unless @md_to_umm_primitive_mapping.empty?
    # Primitives defined by the OMG UML specification
    # All primitive types that are used in the model by not defined in the model (e.g. those defined by UML -- Integer, Boolean, Null, Real, String, UnlimitedNatural, etc.. OR those those defined by the modeling tool such as those defined by UML Standard Profile::MagicDraw Profile::datatypes)

    md_umlp = MagicDraw.uml_primitives
    # md_uml_null = find_md_element_by_name("Null", md_uml_primitives) # MagicDraw doesn't have this even though UML does.  Go Figure...

    @md_to_umm_primitive_mapping[md_umlp[:integer]]          = UmlMetamodel::PRIMITIVES[:integer]
    @md_to_umm_primitive_mapping[md_umlp[:real]]             = UmlMetamodel::PRIMITIVES[:real]
    @md_to_umm_primitive_mapping[md_umlp[:string]]           = UmlMetamodel::PRIMITIVES[:string]
    @md_to_umm_primitive_mapping[md_umlp[:boolean]]          = UmlMetamodel::PRIMITIVES[:boolean]
    @md_to_umm_primitive_mapping[md_umlp[:unlimitednatural]] = UmlMetamodel::PRIMITIVES[:unlimited_natural]

    # Primitives specific to MagicDraw
    md_mdpd = MagicDraw.md_profile_datatypes
    # TODO: These two aren't mapped
    # md_mdpd[:structuredexpression]
    # md_mdpd[:xml]

    @md_to_umm_primitive_mapping[md_mdpd[:int]]     = UmlMetamodel::PRIMITIVES[:integer]
    @md_to_umm_primitive_mapping[md_mdpd[:long]]    = UmlMetamodel::PRIMITIVES[:integer]
    @md_to_umm_primitive_mapping[md_mdpd[:short]]   = UmlMetamodel::PRIMITIVES[:integer]
    @md_to_umm_primitive_mapping[md_mdpd[:byte]]    = UmlMetamodel::PRIMITIVES[:byte]
    @md_to_umm_primitive_mapping[md_mdpd[:char]]    = UmlMetamodel::PRIMITIVES[:char]
    @md_to_umm_primitive_mapping[md_mdpd[:date]]    = UmlMetamodel::PRIMITIVES[:date]
    @md_to_umm_primitive_mapping[md_mdpd[:float]]   = UmlMetamodel::PRIMITIVES[:float]
    @md_to_umm_primitive_mapping[md_mdpd[:double]]  = UmlMetamodel::PRIMITIVES[:float]
    @md_to_umm_primitive_mapping[md_mdpd[:boolean]] = UmlMetamodel::PRIMITIVES[:boolean]
    @md_to_umm_primitive_mapping[md_mdpd[:void]]    = UmlMetamodel::PRIMITIVES[:null]
    
    # Auxiliary Primitives defined in UML package (may or may not be defined in the model)
    mda_p = MagicDrawAuxiliary.primitives
    @md_to_umm_primitive_mapping[mda_p[:datetime]]          = UmlMetamodel::PRIMITIVES[:datetime] if mda_p[:datetime]
    @md_to_umm_primitive_mapping[mda_p[:regularexpression]] = UmlMetamodel::PRIMITIVES[:regular_expression] if mda_p[:regularexpression]
    @md_to_umm_primitive_mapping[mda_p[:time]]              = UmlMetamodel::PRIMITIVES[:time] if mda_p[:time]
    @md_to_umm_primitive_mapping[mda_p[:uri]]               = UmlMetamodel::PRIMITIVES[:uri] if mda_p[:uri]
    
    @md_to_umm_primitive_mapping
  end
  
  def self.umm_to_md_primitive_mapping(reload = false)
    @umm_to_md_primitive_mapping ||= {}
    @umm_to_md_primitive_mapping = {} if reload
    return @umm_to_md_primitive_mapping unless @umm_to_md_primitive_mapping.empty?
    # Primitives defined by the OMG UML specification
    # All primitive types that are used in the model by not defined in the model (e.g. those defined by UML -- Integer, Boolean, Null, Real, String, UnlimitedNatural, etc.. OR those those defined by the modeling tool such as those defined by UML Standard Profile::MagicDraw Profile::datatypes)
    md_umlp = MagicDraw.uml_primitives
    # md_uml_null     = find_md_element_by_name "Null", md_uml_primitives # MagicDraw doesn't have this even though UML does.  Go Figure...

    @umm_to_md_primitive_mapping[UmlMetamodel::PRIMITIVES[:integer]]           = md_umlp[:integer]
    @umm_to_md_primitive_mapping[UmlMetamodel::PRIMITIVES[:real]]              = md_umlp[:real]
    @umm_to_md_primitive_mapping[UmlMetamodel::PRIMITIVES[:string]]            = md_umlp[:string]
    @umm_to_md_primitive_mapping[UmlMetamodel::PRIMITIVES[:boolean]]           = md_umlp[:boolean]
    @umm_to_md_primitive_mapping[UmlMetamodel::PRIMITIVES[:unlimited_natural]] = md_umlp[:unlimitednatural]

    # Primitives specific to MagicDraw
    md_mdpd = MagicDraw.md_profile_datatypes
    # TODO: These two aren't mapped
    # md_mdpd[:structuredexpression]
    # md_mdpd[:xml]
    
    @umm_to_md_primitive_mapping[UmlMetamodel::PRIMITIVES[:byte]]  = md_mdpd[:byte]
    @umm_to_md_primitive_mapping[UmlMetamodel::PRIMITIVES[:char]]  = md_mdpd[:char]
    @umm_to_md_primitive_mapping[UmlMetamodel::PRIMITIVES[:date]]  = md_mdpd[:date]
    @umm_to_md_primitive_mapping[UmlMetamodel::PRIMITIVES[:float]] = md_mdpd[:float]
    @umm_to_md_primitive_mapping[UmlMetamodel::PRIMITIVES[:null]]  = md_mdpd[:void]
    
    # TODO -- The stuff that is commented out here represents stuff that we don't support in UmlMetamodel.  We will hear about it if [long|short|double] end up being important to somebody.
    # @umm_to_md_primitive_mapping[UmlMetamodel::PRIMITIVES[:float]] = md_mdpd[:double]
    # @umm_to_md_primitive_mapping[UmlMetamodel::PRIMITIVES[:integer]] = md_mdpd[:long]
    # @umm_to_md_primitive_mapping[UmlMetamodel::PRIMITIVES[:integer]] = md_mdpd[:short]
       
    # These just shouldn't be used!
    # @umm_to_md_primitive_mapping[UmlMetamodel::PRIMITIVES[:integer]] = md_mdpd[:int]
    # @umm_to_md_primitive_mapping[UmlMetamodel::PRIMITIVES[:boolean]] = md_mdpd[:boolean]
    
    # Auxiliary Primitives defined in UML package (may or may not be defined in the model)
    mda_p = MagicDrawAuxiliary.primitives
    @umm_to_md_primitive_mapping[UmlMetamodel::PRIMITIVES[:datetime]]           = mda_p[:datetime] if mda_p[:datetime]
    @umm_to_md_primitive_mapping[UmlMetamodel::PRIMITIVES[:regular_expression]] = mda_p[:regularexpression] if mda_p[:regularexpression]
    @umm_to_md_primitive_mapping[UmlMetamodel::PRIMITIVES[:time]]               = mda_p[:time] if mda_p[:time]
    @umm_to_md_primitive_mapping[UmlMetamodel::PRIMITIVES[:uri]]                = mda_p[:uri] if mda_p[:uri]
    
    @umm_to_md_primitive_mapping
  end
end