# must not have the word m-o-d-u-l-e above the next line (so that a Regexp can figure out the m-o-d-u-l-e name)
module UmmAdapter
  
  # For more information about meta_info.rb, please see project MM, lib/mm/meta_info.rb
  
  # Required String
  GEM_NAME = "umm_adapter"
  # Required String
  VERSION = '0.1.0'
  # Optional String or Array of Strings
  AUTHORS = ["Michael Faughn"]
  # Optional String or Array of Strings
  EMAILS = ["m.faughn@prometheuscomputing.com"]
  # Optional String
  HOMEPAGE = nil
  # Required String
  SUMMARY = %q{UmlMetamodel objects that facilitate the storing of metainformation and the use of auxillary primitive types}
  # Optional String
  DESCRIPTION = %q{UmlMetamodel objects that facilitate the storing of metainformation and the use of auxillary primitive types}
  
  # Required Symbol
  # This specifies the language the project is written in (not including the version, which is in LANGUAGE_VERSION).
  # A project should only have one LANGUAGE (not including, for example DSLs such as templating languages).
  # If a project has more than one language (not including DSLs), it should be split.
  # The reason is that mixing up languages in one project complicates packaging, deployment, metrics, directory structure, and many other aspects of development.
  # Choices are currently:
  #   * :ruby - project contains ZERO java code
  #           it may contain JRuby code and depend or jars or :java projects,  if RUNTIME_VERSIONS has a :jruby key
  #           implies packaging as gem
  #   * :java - contains ZERO ruby code (with exception of meta_info.rb), and depends on zero Ruby code.
  #           implies packaging as jar - may eventually also support ear, war, sar, etc
  LANGUAGE = :ruby
  # This differs from Runtime version - this specifies the version of the syntax of LANGUAGE
  LANGUAGE_VERSION = ['> 2.3.0']
  # This is different from aGem::Specification.platform, which appears to be concerned with OS.
  # This defines which implentation of Ruby, Java, etc can be used.
  # Required Hash, in same format as DEPENDENCIES_RUBY.
  # The version part is used by required_ruby_version
  # Allowable keys depend on LANGUAGE. They are in VALID_<language.upcase>_RUNTIMES
  RUNTIME_VERSIONS = {
    :mri => ['>= 1.9.2'],
    :jruby => ['>= 1.7.19']
  }
  # DEPRECATGED Symbol - code is now looking at APP_TYPES instead of this.
  # Choices are currently:
  #   * :library - reusable functionality, not intended to stand alone. Might be a plugin.
  #   * :utility - intended for use on command line
  #   * :web_app - an application that uses a web browser for it's GUI
  #   * :service - listens on some port. May include command line tools to manage the server.
  #   * :gui_app - has a Swing, Fox, WXwidget, etc GUI
  TYPE = :utility
  # Array of Symbols specifying what type of app to build. Empty for no Apps.
  # If LANGUAGE is ruby, you can use :zip, :jar, :appBundler, or :platypus
  # If LANGUAGE is java, you can use :zip, :jar, :appBundler
  #   * :jar        - A jar containing all gems and the contents of all jars. To avoid problems with signed code, may need to change to http://one-jar.sourceforge.net/, http://code.google.com/p/jarjar/, JarSplice, etc
  #   * :zip        - A zip file containing all required jars, and all required unpacked gems. Can run app by catapult.rb
  #   * :appBundler - Makes a Windows .exe, and a .zip of a Mac application.  In both cases, the application is based on a jar.
  #   * :platypus   - A binary made by Platypus from the contents of :zip. Does not yet include Ruby. Uses whatever ruby is specified by "which ruby"
  APP_TYPES = []
  # Specifies what to invoke when the user runs it. Not meaningful for libraries (which are invoked by API) or utilities (invoked by shell script in 'bin' directory).
  # When :ruby==LANGUAGE
  #    LAUNCHER must be a String path, relative to either bin or lib. If present in both, bin is used.
  #    If in bin, this will be invoked through the operating system (after setting up PATH and CLASSPATH).
  # When :java==LANGUAGE
  #    LAUNCHER must be a String path, relative to lib, that specifies Main-Class in the jar manifest.
  LAUNCHER = nil
  
  # Optional Hashes - if nil, presumed to be empty.
  # There may be additional dependencies, for platforms (such as :maglev) other than :mri and :jruby
  # If JRuby platform Ruby code depends on a third party Java jar, that goes in DEPENDENCIES_JAVA
  DEPENDENCIES_RUBY = { :uml_metamodel => '~> 3.0', :magicdraw_extensions => '~> 0.13' }
  DEPENDENCIES_MRI = { }
  DEPENDENCIES_JRUBY = { }
  DEVELOPMENT_DEPENDENCIES_RUBY = { } 
  DEVELOPMENT_DEPENDENCIES_MRI = { }
  DEVELOPMENT_DEPENDENCIES_JRUBY = { }
  
  # Java dependencies are harder to handle because Java does not have an equivalent of Ruby gem servers.
  # The closest thing is Maven repositories, which are growing in popularity, but not yet ubiquitous enough to warrant supporting in this tool.
  # Currently only the keys are used, version info is ignored.
  # Keys can be the names of Jars (complete, including any version info embedded in name) that must be located in JARCHIVE (key must end in .jar), or the name of a constant (key must not end in .jar).
  # Constsants must be defined in this module. Constants must not be computed from absolute paths (use environmental variables if necessary).
  # Support for constants is provided primarily to accomodate MagicDraw, which requires a large number of jars.
  DEPENDENCIES_JAVA = { }
  DEPENDENCIES_JAVA_SE = { }
  DEPENDENCIES_JAVA_ME = { }
  DEPENDENCIES_JAVA_EE = { }
  
  # An Array of strings that YARD will interpret as regular expressions of files to be excluded.
  YARD_EXCLUDE = []
  
end
