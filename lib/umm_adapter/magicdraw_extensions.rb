require 'magicdraw_extensions/helpers'
require 'magicdraw_extensions/magic_draw_stereotypes'

module MagicDraw
  def self.uml_metamodel_adapter
    find_package_named('UmlMetamodelAdapter', [root_model])
  end

  def self.generation_options_stereotype
    uml_metamodel_adapter.get_stereotype('generation_options')
  end

  def self.metainformation_stereotype
    uml_metamodel_adapter.get_stereotype('metainformation')
  end
end
